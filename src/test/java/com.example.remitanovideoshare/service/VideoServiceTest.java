package com.example.remitanovideoshare.service;

import com.example.remitanovideoshare.entity.Video;
import com.example.remitanovideoshare.repository.VideoRepo;
import com.example.remitanovideoshare.repository.VideoRepoCustom;
import com.example.remitanovideoshare.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
public class VideoServiceTest {

	@Test
	public void testValidate_success() {
		VideoRepo videoRepo = Mockito.mock(VideoRepo.class);
		VideoRepoCustom videoRepoCustom = Mockito.mock(VideoRepoCustom.class);
		VideoService videoService = new VideoService(videoRepo, videoRepoCustom);

		Video video = Video.builder()
				.title("title").sharedBy("dung")
				.numLike(1l).numDislike(1l).link("https://google.com.vn").description("good")
				.icon("https://icon.com.vn")
				.build();
		String errorMessage = videoService.validate(video);
		Assert.assertNull(errorMessage);
	}

	@Test
	public void testValidate_link_empty_return_error_LINK_EMPTY() {
		VideoRepo videoRepo = Mockito.mock(VideoRepo.class);
		VideoRepoCustom videoRepoCustom = Mockito.mock(VideoRepoCustom.class);
		VideoService videoService = new VideoService(videoRepo, videoRepoCustom);

		Video video = Video.builder()
				.title("title").sharedBy("dung")
				.numLike(1l).numDislike(1l).link(null).description("good")
				.icon("https://icon.com.vn")
				.build();
		String errorMessage = videoService.validate(video);
		Assert.assertEquals(Constants.SHARE_VIDEO.LINK_EMPTY, errorMessage);
	}


}
