create table video
(
    id          integer primary key auto_increment not null,
    title       varchar(255) not null,
    num_like    integer,
    num_dislike integer,
    description varchar(255),
    link        varchar(255),
    shared_by   varchar(255)
);
