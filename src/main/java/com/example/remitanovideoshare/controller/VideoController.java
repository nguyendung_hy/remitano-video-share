package com.example.remitanovideoshare.controller;

import com.example.remitanovideoshare.entity.*;
import com.example.remitanovideoshare.lazymodel.VideoLazyModel;
import com.example.remitanovideoshare.service.SessionService;
import com.example.remitanovideoshare.service.UserService;
import com.example.remitanovideoshare.service.VideoService;
import com.example.remitanovideoshare.utils.Constants;
import com.example.remitanovideoshare.utils.MessageUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Slf4j
@Data
@Scope(value = "session")
@Component(value = "videoController")
@ELBeanName(value = "videoController")
@Join(path = "/", to = "/layout.jsf")
public class VideoController {
	@Autowired
	private VideoService videoService;

	@Autowired
	private UserService userService;

	@Autowired
	private SessionService sessionService;

	private Video sharedVideo;

	private RegisterRequest registerRequest;

	private LoginRequest loginRequest;

	private Session session;

	private LazyDataModel<Video> lazyModel;

	private VideoSearchForm videoSearchForm;


	private static final String registerMessageId = "registerForm\\\\:messageValidate";
	private static final String loginMessageId = "loginForm\\\\:messageValidate";

	private static final String shareVideoMessageId = "shareVideoForm\\\\:messageValidate";


	@PostConstruct
	public void init() {
		log.info("init video controller");
		registerRequest = new RegisterRequest();
		loginRequest = new LoginRequest();
		session = null;
		videoSearchForm = new VideoSearchForm();
		lazyModel = new VideoLazyModel(videoService, videoSearchForm);
	}

	public void prepareLogin() {
		log.info("start prepare login");
		loginRequest = new LoginRequest();
	}

	public void prepareRegister() {
		log.info("start prepare register");
		registerRequest = new RegisterRequest();
	}

	public void prepareShareVideo() {
		log.info("start share video");
		sharedVideo = new Video();
	}


	public boolean login() {
		log.info("Start login");
		String errMessage = userService.login(loginRequest);
		if (!StringUtils.isEmpty(errMessage)) {
			MessageUtils.setMessageForMessageId(loginMessageId, errMessage, "error");
			return false;
		}

		session = Session.builder()
				.session(UUID.randomUUID().toString())
				.username(loginRequest.getUsername())
				.expiredTime(System.currentTimeMillis() + Constants.EXPIRED_TIME)
				.status(Constants.ACTIVE)
				.build();
		sessionService.save(session);

		MessageUtils.setMessageForMessageId(loginMessageId, "Login success", "info");
		return true;
	}

	public boolean register() {
		log.info("start register");
		String errMessage = userService.register(registerRequest);
		if (!StringUtils.isEmpty(errMessage)) {
			MessageUtils.setMessageForMessageId(registerMessageId, errMessage, "error");
			return false;
		}

		MessageUtils.setMessageForMessageId(registerMessageId, "Register success", "info");
		return true;
	}

	public boolean shareVideo() {
		log.info("share a video");
		if (session != null) {
			sharedVideo.setSharedBy(session.getUsername());
			String errMessage = videoService.save(sharedVideo);
			if (!StringUtils.isEmpty(errMessage)) {
				MessageUtils.setMessageForMessageId(shareVideoMessageId, errMessage, "error");
				return false;
			}

			MessageUtils.setMessageForMessageId(shareVideoMessageId, "Share video success", "info");
			return true;
		}
		return false;
	}

	public boolean searchVideo() {
		log.info("Start search video");
		lazyModel = new VideoLazyModel(videoService, videoSearchForm);
		log.info("videoSearchForm: "+ videoSearchForm);
		return false;
	}

	public boolean logout() {
		log.info("start logout");
		sessionService.remove(session);
		session = null;
		return true;
	}

}
