package com.example.remitanovideoshare.lazymodel;

import com.example.remitanovideoshare.entity.Video;
import com.example.remitanovideoshare.entity.VideoSearchForm;
import com.example.remitanovideoshare.service.VideoService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class VideoLazyModel extends LazyDataModel<Video> {
	private VideoService videoService;
	private List<Video> listVideo;
	private int dataSize;
	private VideoSearchForm videoSearchForm;

	public VideoLazyModel(VideoService videoService){
		this.videoService = videoService;
	}
	public VideoLazyModel(VideoService videoService, VideoSearchForm videoSearchForm){
		this.videoService = videoService;
		this.videoSearchForm = videoSearchForm;
	}
	@Override
	public Video getRowData(String rowKey) {
		return videoService.getById(Long.valueOf(rowKey));
	}

	@Override
	public Object getRowKey(Video video) {
		return video.getId();
	}

	@Override
	public List<Video> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		try {
			int page = first / pageSize ;
			List<Video> listVideo = videoService.findVideo(videoSearchForm, page, pageSize, sortField, sortOrder.toString());
			dataSize = videoService.countVideo(videoSearchForm);
			this.setRowCount(dataSize);
			return listVideo;
		} catch (Exception e) {
			log.error("error ", e);
		}
		return new ArrayList<>();
	}
}
