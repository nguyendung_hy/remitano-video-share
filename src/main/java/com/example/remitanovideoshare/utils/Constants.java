package com.example.remitanovideoshare.utils;

public class Constants {


    public static final class SHARE_VIDEO{

		public static final String LINK_EMPTY = "Link is required";
		public static final String ICON_EMPTY = "Icon is required";
		public static final String TITLE_EMPTY = "Title is required";
		public static final String DESCRIPTION_EMPTY = "Description is required";
		public static final String SHARED_BY_EMPTY = "Shared user is required";
	}

    private Constants() {
	}

	public static final class REGISTER {
		public static final String USER_PASS_EMPTY = "User name or password empty";
		public  static final String CONFIRM_PASSWORD_NOT_MATCH= "Password and confirm password not matched";

		public  static final String USER_EXISTED= "User existed";;

	}

	public static final class LOGIN{

		public static final String INVALID_USER_PASS = "Invalid user or password";
		public static final String USER_PASS_EMPTY = "User name or password empty";
	}

	public static final long EXPIRED_TIME = 60 * 60 * 1000;

	public static final Long ACTIVE = 1L;
	public static final Long IN_ACTIVE = 0L;



}
