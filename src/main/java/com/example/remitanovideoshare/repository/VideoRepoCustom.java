package com.example.remitanovideoshare.repository;

import com.example.remitanovideoshare.entity.Video;
import com.example.remitanovideoshare.entity.VideoSearchForm;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VideoRepoCustom  {

	List<Video> findVideo(VideoSearchForm form, int page, int pageSize, String fieldOrder, String orderValue) throws Exception;

	int countVideo(VideoSearchForm form) throws Exception;
}
