package com.example.remitanovideoshare.repository;

import com.example.remitanovideoshare.entity.Video;
import com.example.remitanovideoshare.entity.VideoSearchForm;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class VideoRepoCustomImpl implements VideoRepoCustom {
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<Video> findVideo(VideoSearchForm form, int page, int pageSize, String fieldOrder, String orderValue) throws Exception {
		String sql = "SELECT id, title,num_like,num_dislike,description,link,shared_by,icon " +
				"FROM video " +
				"WHERE 1 = 1 ";

		List<Object> params = new ArrayList<>();
		if (!StringUtils.isEmpty(form.getTitle())) {
			sql += "AND title LIKE  ? ";
			params.add("%" + form.getTitle() + "%");
		}

		if (!StringUtils.isEmpty(form.getDescription())) {
			sql += "AND description LIKE ? ";
			params.add("%" + form.getDescription() + "%");
		}

		if (!StringUtils.isEmpty(form.getSharedBy())) {
			sql += "AND shared_by LIKE ? ";
			params.add("%" + form.getSharedBy() + "%");
		}

		if (page >= 0 && pageSize > 0) {
			sql += "LIMIT ? OFFSET ? ";
			params.add(pageSize);
			params.add(pageSize * page);
		}

		if (!StringUtils.isEmpty(fieldOrder) && !StringUtils.isEmpty(orderValue)) {
			sql += "ORDER BY " + fieldOrder + " " + orderValue;
		}

		Query query = entityManager.createNativeQuery(sql, Video.class);
		for (int i = 0; i < params.size(); i++) {
			query.setParameter(i + 1, params.get(i));
		}

		List<Video> list = query.getResultList();
		return list;
	}

	@Override
	public int countVideo(VideoSearchForm form) throws Exception {
		String sql = "SELECT count(id) " +
				"FROM video " +
				"WHERE 1 = 1 ";

		List<Object> params = new ArrayList<>();
		if (!StringUtils.isEmpty(form.getTitle())) {
			sql += "AND title LIKE ? ";
			params.add("%" + form.getTitle() + "%");
		}

		if (!StringUtils.isEmpty(form.getDescription())) {
			sql += "AND description LIKE ? ";
			params.add("%" + form.getDescription() + "%");
		}

		if (!StringUtils.isEmpty(form.getSharedBy())) {
			sql += "AND shared_by LIKE ? ";
			params.add("%" + form.getSharedBy() + "%");
		}


		Query query = entityManager.createNativeQuery(sql);
		for (int i = 0; i < params.size(); i++) {
			query.setParameter(i + 1, params.get(i));
		}

		Long count = new Long(query.getSingleResult().toString());
		return count.intValue();
	}

}
