package com.example.remitanovideoshare.repository;

import com.example.remitanovideoshare.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionRepo extends JpaRepository<Session,Long> {

}
