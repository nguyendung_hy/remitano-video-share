package com.example.remitanovideoshare.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "video")
public class Video {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String title;

	@Column
	private Long numLike;

	@Column
	private Long numDislike;

	@Column
	private String description ;

	@Column
	private String link ;

	@Column
	private String icon ;

	@Column
	private String sharedBy ;




}
