package com.example.remitanovideoshare.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class VideoSearchForm {

	private String title;

	private String description;

	private String sharedBy;

}
