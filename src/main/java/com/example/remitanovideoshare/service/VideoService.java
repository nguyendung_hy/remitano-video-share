package com.example.remitanovideoshare.service;

import com.example.remitanovideoshare.entity.Video;
import com.example.remitanovideoshare.entity.VideoSearchForm;
import com.example.remitanovideoshare.repository.VideoRepo;
import com.example.remitanovideoshare.repository.VideoRepoCustom;
import com.example.remitanovideoshare.utils.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@Data
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class VideoService {

	@Autowired
	private VideoRepo videoRepo;

	@Autowired
	private VideoRepoCustom videoRepoCustom;


	public List<Video> findAll() {
		return videoRepo.findAll();
	}

	public Video getById(Long id) {
		return videoRepo.getOne(id);
	}

	public String save(Video video) {
		String errMessage = validate(video);
		if (!StringUtils.isEmpty(errMessage)) {
			return errMessage;
		}
		videoRepo.save(video);
		return null;
	}

	public String validate(Video video) {
		if (video == null) {
			return Constants.SHARE_VIDEO.LINK_EMPTY;
		}

		if (StringUtils.isEmpty(video.getLink())) {
			return Constants.SHARE_VIDEO.LINK_EMPTY;
		}

		if (StringUtils.isEmpty(video.getIcon())) {
			return Constants.SHARE_VIDEO.ICON_EMPTY;
		}

		if (StringUtils.isEmpty(video.getTitle())) {
			return Constants.SHARE_VIDEO.TITLE_EMPTY;
		}

		if (StringUtils.isEmpty(video.getDescription())) {
			return Constants.SHARE_VIDEO.DESCRIPTION_EMPTY;
		}

		if (StringUtils.isEmpty(video.getSharedBy())) {
			return Constants.SHARE_VIDEO.SHARED_BY_EMPTY;
		}

		return null;
	}

	public List<Video> findVideo(VideoSearchForm form, int page, int pageSize, String fieldOrder, String orderValue)   {
		try {
			return videoRepoCustom.findVideo(form, page, pageSize, fieldOrder, orderValue);
		} catch (Exception e) {
			log.error("have error ", e);
		}
		return new ArrayList<>();
	}

	public int countVideo(VideoSearchForm form)   {
		try {
			return videoRepoCustom.countVideo(form);
		} catch (Exception e) {
			log.error("have error",e);
		}
		return 0;
	}

}
