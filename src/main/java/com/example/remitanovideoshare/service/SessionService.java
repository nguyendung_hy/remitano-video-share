package com.example.remitanovideoshare.service;

import com.example.remitanovideoshare.entity.Session;
import com.example.remitanovideoshare.repository.SessionRepo;
import com.example.remitanovideoshare.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SessionService {
	@Autowired
	private SessionRepo sessionRepo;

	public void save(Session session){
		sessionRepo.save(session);
	}

	public void remove(Session session){
		if(session == null || session.getId() == null){
			return;
		}
		session.setStatus(Constants.IN_ACTIVE);
		sessionRepo.save(session);
	}

}
