FROM openjdk:11
COPY target/remitano-video-share-1.0-SNAPSHOT.war /home/remitano-video-share-1.0-SNAPSHOT.war
CMD ["java","-jar","/home/remitano-video-share-1.0-SNAPSHOT.war"]
